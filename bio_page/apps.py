from django.apps import AppConfig


class BioPageConfig(AppConfig):
    name = 'bio_page'
