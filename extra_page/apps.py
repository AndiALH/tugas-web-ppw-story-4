from django.apps import AppConfig


class ExtraPageConfig(AppConfig):
    name = 'extra_page'
