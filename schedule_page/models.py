from django.db import models

DAY = [
    ('Monday', 'Monday'),
    ('Tuesday', 'Tuesday'),
    ('Wednesday', 'Wednesday'),
    ('Thursday', 'Thursday'),
    ('Friday', 'Friday'),
    ('Saturday', 'Saturday'),
    ('Sunday', 'Sunday'),
]

# Create your models here.
class Activity(models.Model):
    activity = models.CharField(max_length = 20)
    category = models.CharField(max_length = 20)
    place = models.CharField(max_length = 20)
    # date_time = models.DateTimeField()
    date = models.DateField()
    time = models.TimeField()
    day = models.CharField(max_length = 9, choices = DAY)

    def __str__(self):
        return "{}".format(self.activity) 
