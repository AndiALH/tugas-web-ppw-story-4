from django import forms

from .models import Activity


class Input_Form(forms.ModelForm):
    # activity = forms.CharField(widget = forms.TextInput({'class':'form'}))
    # category = forms.CharField(widget = forms.TextInput({'class':'form'}))
    # place = forms.CharField(widget = forms.TextInput({'class':'form'}))
    # date_time = forms.DateTimeField(widget =  forms.DateTimeInput({'class':'form'}))
    # date = forms.DateField(widget = forms.DateInput({'class':'form'}))
    # time = forms.TimeField(widget = forms.TimeInput({'class':'form'}))
    # day = forms.CharField(widget = forms.TextInput({'class':'form'}))
    class Meta:
        model = Activity
        fields = '__all__'
        widgets = {
            'activity' : forms.TextInput(
                attrs = {
                    'class' : 'form-control'
                }
            ),

            'category' : forms.TextInput(
                attrs = {
                    'class' : 'form-control'
                }
            ),

            'place' : forms.TextInput(
                attrs = {
                    'class' : 'form-control'
                }
            ),

            'date' : forms.SelectDateWidget(
                attrs = {
                    'class' : 'form-control'
                }
            ),

            'time' : forms.TimeInput(
                attrs = {
                    'type' : 'time',
                    'class' : 'form-control'
                }
            ),

        }
