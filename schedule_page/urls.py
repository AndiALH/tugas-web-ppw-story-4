from django.urls import path
from . import views

app_name = 'schedule_page'

urlpatterns = [
    path('',views.schedule_page,name='schedule'),
    path('create/',views.create,name='create'),
    path('delete/<int:id_delete>',views.delete,name='delete'),
    # path('',include('tugas.urls')),
]