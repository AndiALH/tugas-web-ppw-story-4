from django.shortcuts import render, redirect
from .models import Activity
from .forms import Input_Form

# Create your views here.


def schedule_page(request):
    activity = Activity.objects.all()

    response = {
        # 'form' : Input_Form(),
        'activity' : activity
    }

    return render(request, 'pages/schedule_page.html', response)


def create(request):
    form = Input_Form(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect('schedule_page:schedule')

    response = {
        'form' : form
    }

    return render(request, 'pages/filling_page.html', response)


def delete(request, id_delete):
    Activity.objects.filter(id = id_delete).delete()
    return redirect('schedule_page:schedule')