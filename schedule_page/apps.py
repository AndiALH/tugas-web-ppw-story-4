from django.apps import AppConfig


class SchedulePageConfig(AppConfig):
    name = 'schedule_page'
