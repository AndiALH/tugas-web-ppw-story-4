from django.apps import AppConfig


class EducationPageConfig(AppConfig):
    name = 'education_page'
